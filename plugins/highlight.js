import VueCodeHighlight from "vue-code-highlight";
import "vue-code-highlight/themes/prism-okaidia.css";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueCodeHighlight);
});
