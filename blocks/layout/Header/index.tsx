import ThemeSwitcher from "~/components/ThemeSwitcher";
import TabSwitcher from "~/components/TabSwitcher";
import type { Router } from "~/components/TabSwitcher";
import "./index.scss";

export default defineComponent({
  setup() {
    const { t } = useT();
    const { goTo } = useJumpRouter();
    const routers: Router[] = [
      {
        key: "0",
        label: t("component"),
        operation: () => goTo("/"),
      },
      {
        key: "1",
        label: t("icon"),
        operation: () => goTo("/icon"),
      },
    ];
    const selectedItem = ref<Router>(routers[0]);
    return () => (
      <div class={"default-layout__header"}>
        <div class="default-layout__header--left-panel">Just-Use-UI</div>
        <TabSwitcher routerList={routers} v-model={selectedItem.value} />
        <div class="default-layout__header--right-panel">
          <ThemeSwitcher />
        </div>
      </div>
    );
  },
});
