import "./index.scss";

export default defineComponent({
  setup() {
    return () => (
      <div class={"default-layout__footer"}>
        <div class="default-layout__footer--left-panel"></div>
        <div class="default-layout__footer--right-panel"></div>
      </div>
    );
  },
});
