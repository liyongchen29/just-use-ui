export type CodeTypes =
  "Icons" |
  "Buttons" |
  "Tabs" |
  "Dialogs" |
  "Menus" |
  "Grids" |
  "Loadings";

export interface ConfirmMessage {
  title: string;
  content: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  useLoading?: boolean;
  slots: any;
}

export interface ResolveType {
  loading: (is: boolean) => void;
}

export interface ConfirmOptions {
  resolver?: (value?: ResolveType) => Promise<any> | void;
  rejecter?: () => void;
}

export interface DialogInject {
  title?: string;
  content?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  useLoading?: boolean;
  slots?: any;
  onConfirm?: (_?: any) => void;
  onOpened?: (_?: any) => void;
  para?: any;
}
