import PreviewCard from "~/components/noGenerate/PreviewCard.vue";
import SliderTab from "~/components/tabs/SliderTab.vue";

export type PreviewCardProp = InstanceType<typeof PreviewCard>["$props"];
export type TabProp = InstanceType<typeof SliderTab>['$props']