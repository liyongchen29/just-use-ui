// https://nuxt.com/docs/api/configuration/nuxt-config
import ElementPlus from "unplugin-element-plus/dist/vite";
export default defineNuxtConfig({
  ssr: true,

  // typescripts
  typescript: {
    strict: true,
    typeCheck: true,
  },

  // plugins
  plugins: ["~/plugins/element-ui.ts", "~/plugins/highlight.js"],
  components: {
    global: true,
    dirs: [
      {
        path: "~/components",
        pathPrefix: false,
      },
      {
        path: "~/components/noGenerate",
        pathPrefix: false,
      },
      {
        path: "~/blocks",
        pathPrefix: false,
      },
    ],
  },

  // vueJsx

  // build
  build: {
    transpile: ["element-plus/es"],
  },
  colorMode: {
    fallback: "dark",
  },
  // modules
  modules: [
    "@pinia/nuxt",
    "@nuxtjs/i18n",
    "@nuxtjs/eslint-module",
    "@nuxtjs/color-mode",
    "@vue-macros/nuxt",
  ],
  css: ["element-plus/dist/index.css", "vue-final-modal/style.css"],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/style/main.scss";',
        },
      },
    },
    plugins: [ElementPlus({})],
    resolve: {
      dedupe: ["vue"],
    },
    build: {
      rollupOptions: {
        external: ["vue"],
        output: {
          globals: {
            vue: "Vue",
          },
        },
      },
    },
  },
  i18n: {
    locales: [
      { code: "en-US", file: "en-US.json" },
      { code: "zh-CN", file: "zh-CN.json" },
    ],
    lazy: true,
    langDir: "lang/",
    defaultLocale: "en-US",
  },
  // experimental features
  experimental: {
    reactivityTransform: false,
  },

  macros: {},

  // auto import components

  // app config
  app: {
    // global transition
    head: {
      title: "Just-Use-UI",
      meta: [
        {
          charset: "utf-8",
        },
      ],
    },
    pageTransition: { name: "page", mode: "out-in" },
    layoutTransition: { name: "layout", mode: "out-in" },
  },
});
