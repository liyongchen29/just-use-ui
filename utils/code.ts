import buttons from "../tools/buttons.json";
import dialogs from "../tools/dialogs.json";
import icons from "../tools/icons.json";
import tabs from "../tools/tabs.json";
import menus from "../tools/menus.json";
import grids from "../tools/grids.json";
import type { CodeTypes } from "~/types/common";

export function getCode(type: CodeTypes | undefined, name: string) {

  switch (type) {
    case "Buttons":
      return (buttons as any)[name];
    case "Dialogs":
      return (dialogs as any)[name];
    case "Icons":
      return (icons as any)[name];
    case "Tabs":
      return (tabs as any)[name];
    case "Menus":
      return (menus as any)[name];
    case "Grids":
      return (grids as any)[name];
    default:
      break;
  }
}
