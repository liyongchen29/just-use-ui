import { defineComponent } from "vue";
import { array, object } from "vue-types";
import style from "./index.module.scss";

export interface Router {
  key: string | number;
  label: string;
  operation?: (_para?: any) => void;
}

export default defineComponent({
  props: {
    routerList: array<Router>().isRequired,
    modelValue: object<Router>(),
  },

  emits: {
    "update:modelValue": (_item: Router) => true,
  },

  setup(props, { emit }) {
    const selected = computed({
      get: () => props.modelValue!.key,

      set: (key: Router["key"]) => {
        props.routerList.map((item) => {
          return item.key === key ? emit("update:modelValue", item) : null;
        });
      },
    });

    const onSelected = (item: Router) => {
      selected.value = item.key;
      item.operation?.();
    };

    return () => (
      <div class={style["tab-switcher__container"]}>
        {props.routerList.map((item: Router) => {
          return (
            <div
              key={item.key}
              class={`${style["tab-switcher__item"]} ${
                selected.value !== item.key || style.actived
              }`}
              onClick={(_: MouseEvent) => onSelected(item)}
            >
              {item.label}
            </div>
          );
        })}
      </div>
    );
  },
});
