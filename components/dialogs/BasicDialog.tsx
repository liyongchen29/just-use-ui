import { ElButton } from "element-plus";
import { VueFinalModal } from "vue-final-modal";

export default defineComponent({
  props: {
    title: {
      type: String,
      default: "Basic-dialog",
    },
  },
  emits: {
    confirm: () => true,
    close: () => true,
  },
  setup(props, { emit }) {
    return () => (
      <VueFinalModal modalId={"basic-dialog"} class={"dialog-container"}>
        <h1 class="dialog-container__title">{props.title}</h1>
        <slot />
        <ElButton onClick={() => emit("confirm")}>t('confirm')</ElButton>
        <ElButton onClick={() => emit("close")}>t('close')</ElButton>
      </VueFinalModal>
    );
  },
});
