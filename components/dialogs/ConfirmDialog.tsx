import { func, string } from "vue-types";
import { VueFinalModal } from "vue-final-modal";
import { ElButton } from "element-plus";

export default defineComponent({
  props: {
    title: string().def("confirm-dialog"),
    confirmButtonText: string().def("confirm"),
    cancelButtonText: string().def("cancel"),
    ok: func(),
  },
  emits: {
    confirm: () => true,
    cancel: () => true,
  },
  setup(props, { emit }) {
    return () => (
      <VueFinalModal class={""} modalId={"confirm-dialog"}>
        <ElButton onClick={() => emit("confirm")}>
          {props.confirmButtonText}
        </ElButton>
        <ElButton onClick={() => emit("cancel")}>
          {props.cancelButtonText}
        </ElButton>
      </VueFinalModal>
    );
  },
});
