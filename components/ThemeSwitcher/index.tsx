import DarkModeIcon from "../icons/DarkModeIcon";
import LightModeIcon from "../icons/LightModeIcon";
import "./index.scss";

export default defineComponent({
  setup() {
    const colorMode = useColorMode();

    const changeTheme = () => {
      if (colorMode.value === "light") {
        colorMode.preference = "dark";
      } else {
        colorMode.preference = "light";
      }
    };
    return () => (
      <div class={"theme-button"} onClick={changeTheme}>
        {colorMode.value === "light" ? <LightModeIcon /> : <DarkModeIcon />}
      </div>
    );
  },
});
