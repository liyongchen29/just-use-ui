export function useJumpRouter() {
  const router = useRouter();
  return {
    goTo: (url: string) =>
      router.push({
        path: url,
      }),
  };
}
