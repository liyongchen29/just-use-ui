import { useModal } from "vue-final-modal";
import ConfirmDialog from "../components/dialogs/ConfirmDialog";
import type { DialogInject } from "~/types/common";
import CodeDialog from "~/components/dialogs/CodeDialog.vue";

export function useFinalModal() {
  return {
    confirm: (option: DialogInject) => {
      const dialog = useModal({
        component: ConfirmDialog,
        attrs: {
          title: option.title,
          onConfirm: () => {
            option.onConfirm?.();
            dialog.close();
          },
          onOpened: () => {
            option.onOpened?.();
          },
          confirmButtonText: option.confirmButtonText,
          cancelButtonText: option.cancelButtonText,
        },
        slots: option.slots,
      });
      return {
        open: () => dialog.open(),
        close: () => dialog.close(),
      };
    },
    code: (option: DialogInject) => {
      const dialog = useModal({
        component: CodeDialog,
        attrs: {
          para: option.para,
        },
      });
      return {
        open: () => dialog.open(),
        close: () => dialog.close(),
        para: option.para,
      };
    },
  };
}
