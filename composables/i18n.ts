export function useT() {
  const nuxt = useNuxtApp();

  const { t } = nuxt.$i18n;
  return {
    t,
  };
}
